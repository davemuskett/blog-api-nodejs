FROM node:8.0.0
LABEL maintainer = "davemuskett@gloosoftware.com"
WORKDIR /home/node/app
COPY ./ ./
RUN npm install
CMD ["node", "index.js"]
USER node